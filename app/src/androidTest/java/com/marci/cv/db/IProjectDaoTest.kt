package com.marci.cv.db

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.marci.cv.db.models.Project
import com.marci.cv.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class IProjectDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: ProjectDatabase
    private lateinit var dao : IProjectDao

    @Before
    fun setup(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ProjectDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.getProjectDao()
    }

    @After
    fun teardown(){
        database.close()
    }


    @Test
    fun insertProjectItem() = runBlockingTest {
        val projectItem = Project("name", "description")
        dao.insertProject(projectItem)

        val allProjectItem = dao.getAllProjects().getOrAwaitValue()

        assertThat(allProjectItem).contains(projectItem)
    }

    /**
     *  not working, because some error with the primary key in Project data class.
     *  IF the key is in the constructor it's working.
     */
    //@Test
    fun deleteProjectItem() = runBlockingTest {
        val projectItem = Project("name", "description")
        dao.insertProject(projectItem)
        dao.deleteProject(projectItem)

        val allProjectItem = dao.getAllProjects().getOrAwaitValue()

        assertThat(allProjectItem).doesNotContain(projectItem)
    }
}