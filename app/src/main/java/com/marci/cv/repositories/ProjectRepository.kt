package com.marci.cv.repositories

import com.marci.cv.db.IProjectDao
import com.marci.cv.db.models.Project
import javax.inject.Inject

class ProjectRepository @Inject constructor(
    private val projectDao: IProjectDao
) {
    suspend fun insertProject(project: Project) = projectDao.insertProject(project)

    suspend fun deleteProject(project: Project) = projectDao.deleteProject(project)

    fun getAllProject() = projectDao.getAllProjects()
}