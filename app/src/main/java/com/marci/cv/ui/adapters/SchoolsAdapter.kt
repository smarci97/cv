package com.marci.cv.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.marci.cv.databinding.SchoolItemBinding
import com.marci.cv.db.models.School

class SchoolsAdapter(private val schools: List<School>) :
    ListAdapter<School, SchoolsAdapter.ViewHolder>(SchoolAdapterDiffCallback()) {

    inner class ViewHolder(private val binding: SchoolItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: School) {
            binding.itemSchool = item
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: SchoolItemBinding = SchoolItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(schools[position])


    override fun getItemCount(): Int = schools.count()
}

class SchoolAdapterDiffCallback : DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean =
        oldItem.major == newItem.major

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean = oldItem == newItem
}