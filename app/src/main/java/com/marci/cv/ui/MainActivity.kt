package com.marci.cv.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.appbar.AppBarLayout
import com.marci.cv.R
import com.marci.cv.databinding.ActivityMainBinding
import com.marci.cv.ui.viewmodels.ContactViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_contact_buttons.view.*


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val contactViewModel: ContactViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = contactViewModel

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        navHostFragment.findNavController()

        coordinateMotion(binding)
        handleFab(binding)
        handleContactBtns(binding)
    }

    private fun coordinateMotion(binding: ActivityMainBinding) {
        val appBarLayout: AppBarLayout = appbar
        val motionLayout: MotionLayout = motion_layout

        val listener = AppBarLayout.OnOffsetChangedListener { abl, verticalOffset ->
            val seekPosition = -verticalOffset / appBarLayout.totalScrollRange.toFloat()
            motionLayout.progress = seekPosition
            if (verticalOffset <= -25) {
                binding.layoutContactButtons.animate()
                    .alpha(0f)
                    .duration = 100
            } else {
                binding.layoutContactButtons.animate()
                    .alpha(1f)
                    .duration = 200
            }
        }

        appBarLayout.addOnOffsetChangedListener(listener)
    }

    private fun handleFab(binding: ActivityMainBinding) {

        var isOpen = false
        val fabOpen = AnimationUtils.loadAnimation(this, R.anim.fab_open)
        val fabClose = AnimationUtils.loadAnimation(this, R.anim.fab_close)
        val fabClock = AnimationUtils.loadAnimation(this, R.anim.fab_rotate_clock)
        val fabAntiClock = AnimationUtils.loadAnimation(this, R.anim.fab_rotate_anticlock)

        binding.layoutContactButtons.fab_menu.setOnClickListener {
            if (isOpen) {
                binding.layoutContactButtons.fab_mail.startAnimation(fabClose)
                binding.layoutContactButtons.fab_phone.startAnimation(fabClose)
                binding.layoutContactButtons.fab_linkedin.startAnimation(fabClose)
                binding.layoutContactButtons.fab_menu.startAnimation(fabAntiClock)

                binding.layoutContactButtons.fab_mail.isClickable = false
                binding.layoutContactButtons.fab_phone.isClickable = false
                binding.layoutContactButtons.fab_linkedin.isClickable = false

                isOpen = false
            } else {
                binding.layoutContactButtons.fab_mail.startAnimation(fabOpen)
                binding.layoutContactButtons.fab_phone.startAnimation(fabOpen)
                binding.layoutContactButtons.fab_linkedin.startAnimation(fabOpen)
                binding.layoutContactButtons.fab_menu.startAnimation(fabClock)

                binding.layoutContactButtons.fab_mail.isClickable = true
                binding.layoutContactButtons.fab_phone.isClickable = true
                binding.layoutContactButtons.fab_linkedin.isClickable = true

                isOpen = true
            }
        }
    }

    private fun handleContactBtns(binding: ActivityMainBinding) {
        val contactViewModel: ContactViewModel by viewModels()
        binding.layoutContactButtons.fab_mail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto: ${contactViewModel.email}")
            startActivity(Intent.createChooser(intent, "Send Mail"))
        }

        binding.layoutContactButtons.fab_phone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${contactViewModel.phoneNumber}}")

            if (isDialerEnabled())
                startActivity(intent)
            else
                Toast.makeText(
                    this,
                    "Phone is not available in this device",
                    Toast.LENGTH_SHORT
                ).show()
        }
        binding.layoutContactButtons.fab_linkedin.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(contactViewModel.linkedIn)
            startActivity(intent)
        }
    }

    private fun isDialerEnabled(): Boolean {
        val tm: TelephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        return tm != null && tm.simState == TelephonyManager.SIM_STATE_READY
    }
}