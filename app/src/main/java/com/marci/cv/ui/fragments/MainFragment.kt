package com.marci.cv.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.marci.cv.R
import com.marci.cv.databinding.FragmentMainBinding
import com.marci.cv.ui.adapters.ProjectsAdapter
import com.marci.cv.ui.adapters.SchoolsAdapter
import com.marci.cv.ui.dialogs.ProjectDetailsDialog
import com.marci.cv.ui.viewmodels.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private val projectViewModel: ProjectViewModel by viewModels()
    private val schoolViewModel: SchoolViewModel by viewModels()
    private val itKnowledgeViewModel: ITKnowledgeViewModel by viewModels()
    private val otherInformationViewModel: OtherInformationViewModel by viewModels()
    private val languagesViewModel: LanguagesViewModel by viewModels()

    private lateinit var schoolsAdapter: SchoolsAdapter
    private lateinit var projectsAdapter: ProjectsAdapter
    lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        binding.layoutKnowledge.knowledgeViewModel = itKnowledgeViewModel
        binding.layoutOtherinfo.viewModel = otherInformationViewModel
        binding.layoutOtherknowledge.viewModel = otherInformationViewModel
        binding.layoutLanguages.item = languagesViewModel

        setSchoolAdapter()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //deleteAllProject()
        insertMyProject()
        setProjectsAdapter()
        updateUI()
    }

    private fun updateUI() {
        setupLayoutSchools()
        setupLayoutProjects()

        binding.tvDevelopers.setOnClickListener {
            requireActivity().supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, AppDetailsFragment())
                .addToBackStack(null)
                .commit()
        }
    }

    private fun setSchoolAdapter() {
        schoolsAdapter = SchoolsAdapter(schoolViewModel.getSchools())
    }

    private fun setProjectsAdapter() {
        projectsAdapter = ProjectsAdapter()
        projectViewModel.getAllProject().observe(viewLifecycleOwner, Observer {
            projectsAdapter.setData(it)
        })
    }

    private fun deleteAllProject() {
        projectViewModel.getAllProject().observe(viewLifecycleOwner, Observer {
            for (item in it) {
                projectViewModel.deleteProject(item)
            }
        })
    }

    private fun insertMyProject() {
        projectViewModel.getAllProject().observe(viewLifecycleOwner, Observer {
            for (itemInMyProjects in projectViewModel.getMyProjectsFromText()) {
                if (!it.contains(itemInMyProjects)) {
                    projectViewModel.insertProject(itemInMyProjects)
                }
            }
        })
    }

    private fun setupLayoutSchools() {
        if (binding.rvSchools.adapter == null) {
            binding.rvSchools.adapter = schoolsAdapter
            binding.rvSchools.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        }
    }


    private fun setupLayoutProjects() {
        if (binding.rvProjects.adapter == null) {
            binding.rvProjects.adapter = projectsAdapter

            val flexboxLayoutManager = FlexboxLayoutManager(context)
            flexboxLayoutManager.flexDirection = FlexDirection.ROW
            flexboxLayoutManager.flexWrap = FlexWrap.WRAP
            binding.rvProjects.layoutManager = flexboxLayoutManager

            projectsAdapter.onShortClick = {
                ProjectDetailsDialog(it).show(parentFragmentManager, "ProjectDetailsDialog")
            }
        }
    }
}