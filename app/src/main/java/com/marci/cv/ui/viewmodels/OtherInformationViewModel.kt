package com.marci.cv.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.marci.cv.db.models.OtherInformation

data class OtherInformationViewModel(
    val scholarship: OtherInformation = OtherInformation(
        "- Erasmus+ Scholarship",
        "Germany, Esslingen am Neckar, 2019/2020/1"),
    val ssa: OtherInformation = OtherInformation(
        "- Scientific Students Associations, 2018",
        "Title of thesis: Motion capture with a module system based on inertial measurement units"),
    val cas: OtherInformation = OtherInformation("- John von Neumann College for Advanced Studies member"),

    val kl1: OtherInformation = OtherInformation("- Test automation"),
    val kl2: OtherInformation = OtherInformation("- Arduino programming (Embedded system)"),
    val kl3: OtherInformation = OtherInformation("- Basic HTML, CSS"),
    val kl4: OtherInformation = OtherInformation("- ASP.NET"),
    val kl5: OtherInformation = OtherInformation("- Android Studio"),
    val kl6: OtherInformation = OtherInformation("- Firebase"),
    val kl7: OtherInformation = OtherInformation("- Basic iOS programming")
) : ViewModel()