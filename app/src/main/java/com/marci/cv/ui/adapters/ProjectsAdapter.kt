package com.marci.cv.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.marci.cv.databinding.ProjectItemBinding
import com.marci.cv.db.models.Project
import kotlinx.android.synthetic.main.project_item.view.*

class ProjectsAdapter : RecyclerView.Adapter<ProjectsAdapter.ViewHolder>() {

    var onShortClick : ((Project) -> Unit)? = null

    inner class ViewHolder(private val binding: ProjectItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Project) {
            binding.project = item
            binding.executePendingBindings()
        }
    }

    private val diffCallBack = object : DiffUtil.ItemCallback<Project>() {
        override fun areItemsTheSame(oldItem: Project, newItem: Project): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Project, newItem: Project): Boolean {
            return oldItem.name == newItem.name && oldItem.description == newItem.description
        }
    }

    private val differ = AsyncListDiffer(this, diffCallBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ProjectItemBinding = ProjectItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(differ.currentList[position])

        holder.itemView.cl_project_container.setOnClickListener {
            onShortClick?.invoke(differ.currentList[position])
        }
    }

    override fun getItemCount(): Int = differ.currentList.count()

    fun setData(list: List<Project>) {
        differ.submitList(list)
        //projects = list
        //notifyDataSetChanged()
    }
}