package com.marci.cv.ui.viewmodels

import androidx.lifecycle.ViewModel

data class ITKnowledgeViewModel(

    val knowledge1Name: String = "Kotlin",
    val knowledge1Level: Int = 80,      // Maximum 100
    val knowledge2Name: String = "Android",
    val knowledge2Level: Int = 80,
    val knowledge3Name: String = "Espresso\ntest",
    val knowledge3Level: Int = 40,
    val knowledge4Name: String = "Room\ndatabase",
    val knowledge4Level: Int = 65,
    val knowledge5Name: String = "C#",
    val knowledge5Level: Int = 70
) : ViewModel()