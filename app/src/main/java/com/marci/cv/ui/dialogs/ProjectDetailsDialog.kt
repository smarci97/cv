package com.marci.cv.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.marci.cv.R
import com.marci.cv.databinding.DialogProjectdetailsBinding
import com.marci.cv.db.models.Project

class ProjectDetailsDialog(val project: Project) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: DialogProjectdetailsBinding =
            DataBindingUtil.inflate(inflater, R.layout.dialog_projectdetails, container, false)
        binding.item = project

        return binding.root
    }
}