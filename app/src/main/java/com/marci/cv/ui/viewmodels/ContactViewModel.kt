package com.marci.cv.ui.viewmodels

import androidx.lifecycle.ViewModel

class ContactViewModel : ViewModel() {

    val firstName = "Marcell"
    val familyName = "Sándorfi"
    val phoneNumber = "+36304878253"
    val email = "sandorfi.marci@gmail.com"
    val linkedIn = "https://www.linkedin.com/in/marcell-s%C3%A1ndorfi-288b09168/"
    val birthDay = "1997.11.22"
    val residence = "Budapest"
}