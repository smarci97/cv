package com.marci.cv.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.marci.cv.db.models.OtherInformation

class LanguagesViewModel : ViewModel() {

    val german = OtherInformation("German", "Intermediate Language exam - B2")
    val english = OtherInformation("English", "Conversational level")
    val hungarian = OtherInformation("Hungarian", "Native")
}