package com.marci.cv.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.marci.cv.db.models.School

class SchoolViewModel : ViewModel(){

    private val schools = arrayListOf(
        School(
            "Obuda University - John von Neumann Faculty of Informatics",
            "BSc in Computer Science and Engineering",
            "Embedded Systems and Mobile Specialization",
            "2016",
            "2020"
        ), School(
            "Esslingen University of Applied Sciences",
            "Software Engineering and Media Informatics",
            startYear = "2019",
            endYear = "2020"
        )
    )

    fun getSchools(): List<School> = schools
}