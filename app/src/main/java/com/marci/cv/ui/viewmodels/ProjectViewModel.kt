package com.marci.cv.ui.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marci.cv.db.models.Project
import com.marci.cv.repositories.ProjectRepository
import kotlinx.coroutines.launch

class ProjectViewModel @ViewModelInject constructor(
    private val projectRepository: ProjectRepository
) : ViewModel() {

    fun getMyProjectsFromText() : List<Project>{
        return listOf(
            Project("Notification Watcher","It is currently under development.\n" +
                    "The application is basically a notification log that displays the title and full text of the application which is sending the notification. I plan to add other features in the future as well."),
            Project("Get daily photos","The application uses the unsplash api, randomly showing images to the user, who can request a new image with the page refresh gesture. The details of the displayed image can also be viewed and opened on the website. The image can be shared using the application."),
            Project("Screen-on Time","The application contains a widget, which shows the aggregate usage per day. The app itself show the list of application and their usage. It is currently under development, so features can be added in the future"),
            Project("The movie db","This is a simple application that uses themoviedb's api to display the budget and cover image of the movie you are looking for."),
            Project("Smart alarm clock application", "The alarm is done with the help of light, and it also has features such as solving the alarm task or location-based alarm."),
            Project("E additives", "E-number search application. It shows the detailed information of an E-number."),
            Project("Combined Dictionary","Aggregated english dictionary application."),
            Project("Medication Tracker","Medicine diary on phone."),
            Project("Speed Limit Exceeded","Calculation of penalty for exceeding the speed limit."),
            Project("Neptun code finder","Find a name for a specific neptun code. You can search by name or by neptun code. It was developed during my university studies."),
            Project("Housekeeper","Sharing housework with roommates. This is an old project that uses google's firebase. It was a team project on an university assignment.")
        )
    }

    fun insertProject(project : Project) = viewModelScope.launch {
        projectRepository.insertProject(project)
    }

    fun deleteProject(project : Project) = viewModelScope.launch {
        projectRepository.deleteProject(project)
    }

    fun getAllProject() : LiveData<List<Project>> = projectRepository.getAllProject()
}