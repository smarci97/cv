package com.marci.cv.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "projects")
data class Project (
    val name : String,
    val description : String,
){
    @PrimaryKey(autoGenerate = true)
    var id  : Int? = null
}
