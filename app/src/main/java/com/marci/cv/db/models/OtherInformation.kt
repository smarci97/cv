package com.marci.cv.db.models

data class OtherInformation (
    val title : String,
    val description : String? = null
)