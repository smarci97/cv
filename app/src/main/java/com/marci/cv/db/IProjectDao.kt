package com.marci.cv.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.marci.cv.db.models.Project

@Dao
interface IProjectDao{

    @Insert
    suspend fun insertProject(project : Project)

    @Delete
    suspend fun deleteProject(project: Project)

    @Query("SELECT * FROM projects")
    fun getAllProjects() : LiveData<List<Project>>
}