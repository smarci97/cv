package com.marci.cv.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.marci.cv.db.models.Project

@Database(
    entities = [Project::class],
    version = 1
)
abstract class ProjectDatabase : RoomDatabase() {

    abstract fun getProjectDao() : IProjectDao
}