package com.marci.cv.db.models

data class School(
    val name: String,
    val major: String? = null,
    val specialization: String? = null,
    val startYear: String,
    val endYear: String,
    val period: String = "$startYear - $endYear"
)